Using CLI tools to develop for Arduino boards
=============================================

This has examples of how to use Command Line Interface (CLI)
tools to develop software for Arduino UNO r3 boards. One can use
CLI tools with or without the Arduino environment of tools,
files, and libraries. This repo has examples of using CLI tools
with and without the Arduino environment of tools, files, and
libraries.

Arduino has figured how to make it possible for non-technical
people to be successful writing embedded software. That is what
makes Arduino special. It is a huge accomplishment.

To make it easier for non-technical people,
Arduino simplified the development process.
That simplification dumbs things down a bit
and limits experienced developers,
keeping them from using the full capability of the hardware.

00-using-arduino-ide
--------------------

This has a program (aka "sketch" to Arduinistas) that uses the
Arduino GUI integrated development environment (IDE). This shows
what we are migrating away from.

01-using-arduino-cli
--------------------

This has exactly the same program (aka "sketch" to Arduinistas)
as 00-using-arduino-ide, but uses Arduino CLI instead of the
Arduino GUI IDE. It still relies on Arduino tools, files, and
libraries.

02-using-avr-gcc-with-avr-headers
---------------------------------

We use avr-gcc tools, files, and libraries and drop using any
Arduino tools, files, and libraries. Using the stock registers
names works and is ugly.

The program is written as a regular C program, except that main
accepts no arguments and does not return.

03-using-avr-gcc-with-better-names-and-avr-headers
--------------------------------------------------

We use layers of better names in the one C file. The names are
much more meaningful, but having them in one C file is ugly.

We use avr-gcc tools, files, and libraries without using any
Arduino tools, files, and libraries.

The program is written as a regular C program, except that main
accepts no arguments and does not return.

04-using-avr-gcc-with-own-headers-and-avr-headers
-------------------------------------------------

The meaningful names of registers are put into separate header
files. This is the file structure I like and typically use when I
can.

We use avr-gcc tools, files, and libraries without using any
Arduino tools, files, and libraries.

The program is written as a regular C program, except that main
accepts no arguments and does not return.

05-using-avr-gcc-with-own-headers-without-avr-headers
-----------------------------------------------------

If I did not have <avr/io.h>, this is how I would set up my
headers to define hardware registers. Sometimes, especially for a
new chip which extends an old chip, I will include the system
header for the old chip with my own definitions for the new
registers.

This might be as good as it gets
if I do not have a standard header file
that defines the registers for a chip.

This has better structure and names than
06-using-avr-gcc-without-own-headers-without-avr-headers.

We use avr-gcc tools, files, and libraries except for avr/io.h
and without using any Arduino tools, files, and libraries.

The program is written as a regular C program, except that main
accepts no arguments and does not return.

06-using-avr-gcc-without-own-headers-without-avr-headers
--------------------------------------------------------

This is definitely not how I want to the code to end up,
but this is how I might begin a project
that uses a microcontroller
for which I do not have a header file
which defines the registers of the microcontroller.

We use avr-gcc tools, files, and libraries except for avr/io.h
and without using any Arduino tools, files, and libraries.

The program is written as a regular C program, except that main
accepts no arguments and does not return.

---

These examples are based on an
[Arduino UNO](https://www.arduino.cc/en/Guide/ArduinoUno)
because the Arduino UNO r3s are
-   [open source hardware](https://en.wikipedia.org/wiki/Open-source_hardware),
-   plentiful,
-   cheap,
-   and not too complex.

I have both a genuine Arduino UNO r3 and some
[legitimate clones from Microcenter](https://www.microcenter.com/product/486544/inland-uno-r3-mainboard).
The Microcenter clones are so good that the Arduino tools can not distinguish
them from the genuine Arduino boards.

Microcenter clones:

good:
-   [cheaper (<$10)](https://www.microcenter.com/product/486544/inland-uno-r3-mainboard) than [genuine Arduino UNO r3 for $22](https://store.arduino.cc/usa/arduino-uno-rev3)
-   behaves _exactly_ the same as genuine Arduino UNO r3
-   have a DIP chip which easy to clip to pins on
-   the Microcontroller is socketed which makes them easy to
    replace or remove for use in other boards.
-   in stock at local retail store (Columbus and Madison Heights)

bad:
-   not as [cheap](https://www.alibaba.com/product-detail/Development-Starter-Kit-ATmega16U2-Board-Atmega328P_62234441536.html) as [buying directly from China](https://www.alibaba.com/trade/search?SearchText=uno+r3), especially in quantity
