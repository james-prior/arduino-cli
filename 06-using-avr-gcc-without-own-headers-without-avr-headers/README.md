Define registers without using <avr/io.h> or my own headers.
============================================================

This is definitely not how I want to the code to end up,
but this is how I might begin a project
that uses a microcontroller
for which I do not have a header file
which defines the registers of the microcontroller.

Good:
-   Nothing is hidden: everything is in one file.
-   Shows the nitty-gritty details of how hardware registers are defined.
-   Done on command line.
-   Does not rely on Arduino development environment.

Bad:
-   Bad structure from having everything in one file.
-   Is less portable and maintainable by not using <avr/io.h>.
-   wait() is uncalibrated.
-   Does not use TDD.
