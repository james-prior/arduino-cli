Arduino CLI
===========

Create Arduino "sketch" with command line interface (CLI) tools.
Everything here is done the Arduino way,
except that command line tools are used
instead of the "integrated development environment" (IDE).

Connect an Arduino UNO r3 to development computer with USB cable
before executing the following.

Execute once:

    ./INSTALL

Execute to create project and put real code in it.

    ./create-blink-project

Execute each time source code (in Blink/Blink.ino) is changed,
to compile and burn (write) that program into 

    ./build
