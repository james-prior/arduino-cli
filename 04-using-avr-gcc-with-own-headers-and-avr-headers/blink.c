#include <stdint.h>
#include "arduino-uno-r3.h"

void wait(const uint32_t n)
{
    for (volatile uint32_t i = 0; i < n; i++)
        ;
}

int main(void)
{
    led_output_direction |= led_output_direction_mask;

    for (;;) {
        led_output_port |=  led_on_mask;
        wait(100000U);
        led_output_port &= ~led_on_mask;
        wait(100000U);
    }
}
