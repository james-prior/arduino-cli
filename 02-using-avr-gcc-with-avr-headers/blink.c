#include <stdint.h>
#include <avr/io.h>

void wait(const uint32_t n)
{
    for (volatile uint32_t i = 0; i < n; i++)
        ;
}

int main(void)
{
    DDRB |= (1 << DDB5);

    for (;;) {
        PORTB |= (1 << PORTB5);
        wait(100000U);
        PORTB &= ~(1 << PORTB5);
        wait(100000U);
    }
}
